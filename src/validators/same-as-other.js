const validateConfirm = (value, expectation) => fields =>
  fields[value] === fields[expectation];
