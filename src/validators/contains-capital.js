const validateContainsCapital = field => fields => /[A-Z]/.test(fields[field]);
