export const validateEmail = field => fields => {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fields[field])
    ? null
    : "Must be an email.";
};
