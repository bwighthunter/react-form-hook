export const composeValidators = validators => fields => {
  return validators.map(validator => validator(fields));
};
