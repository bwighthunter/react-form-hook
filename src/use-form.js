import React, { useState } from "react";

// warn about not having a required "name" attribute on their input
// some inspiration taken from https://github.com/BenMagyar/form-hooks/
const warnNoName = f => console.log(`${f} called without a "name" on input`);

export const useForm = ({
  initialValues,
  fieldValidators,
  onSubmit,
  validateOnChange = false,
  validateOnBlur = false
}) => {
  const [values, setValues] = useState(initialValues);
  const [validators] = useState(fieldValidators);
  const [errors, setErrors] = useState({});
  const [touched, setTouched] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  const value = field => {
    const { checked, type, value } = field;

    // https://github.com/jaredpalmer/formik/blob/348f44a3016113d6e2b70db714739804ad0ed4c4/src/Formik.tsx#L321
    const parsed = parseFloat(value);
    const isParsed = isNaN(parsed) ? "" : parsed;

    return /number|range/.test(type)
      ? isParsed
      : /checkbox/.test(type)
      ? checked
      : value;
  };

  const handleValidate = name => {
    return Promise.resolve(validators[name](values[name], values)).then(
      errors => setErrors(errors)
    );
  };

  const shouldValidate = touchedFields => {
    const initialFields = Object.keys(initialValues);
    return initialFields.every(f => touchedFields.indexOf(f) > -1);
  };

  const handleBlur = e => {
    const { name } = e.target;

    if (!name) {
      warnNoName("handleBlur");
    }

    setTouched({ ...touched, [name]: true });

    if (validateOnBlur) {
      if (shouldValidate([...Object.keys(touched), name])) {
        handleValidate();
      }
    }
  };

  const handleChange = e => {
    const { target } = e;
    const { name } = target;

    if (!name) {
      return warnNoName("onChange");
    }

    setValues({ ...values, [name]: value(target) });

    if (validateOnChange && shouldValidate(Object.keys(touched))) {
      const errors = validators[name](value);
      const hasError = validators.length > 0;

      setErrors({
        ...errors,
        ...(hasError && { [name]: errors })
      });
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    setIsSubmitting(true);
    const fields = Object.keys(values);
    const hasError = fields.some(key => errors[key] && errors[key].length > 0);

    if (hasError) {
      return errors;
    }

    onSubmit(values);
    return reset();
  };

  const reset = () => {
    setValues(initialValues);
    setErrors({});
    setTouched({});
    setIsSubmitting(false);
  };

  return {
    errors,
    touched,
    values,
    isSubmitting,
    handleChange,
    handleSubmit,
    handleBlur
  };
};
